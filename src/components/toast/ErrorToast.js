import React, { Component } from 'react'
import { Text, View, Image, Dimensions, StyleSheet } from 'react-native'
import Colors from '../../styles/Colors'

const {width} = Dimensions.get('window');


const ErrorToast = (props) => {
    return (
        <View style={styles.errorContainer} >
            <View style={{}} >
                <Image
                    source={require('../../../assets/error.png')}
                    style={styles.image}
                />
            </View>
            <Text style={styles.errorText} >{props.message}</Text>
        </View>
    )
}

export default ErrorToast


const styles = StyleSheet.create({
    errorText: {
        fontSize: 12,
        fontFamily: 'Montserrat-Medium', 
        color: Colors.BLACK, 
        marginLeft: 6, 
        marginRight: 10,
    },
    errorContainer: {
        height: 30,
        marginBottom: 20, 
        flexDirection: 'row', 
        alignItems: 'center', 
        justifyContent: 'center', 
        borderRadius: 6, 
        padding: 4,
        backgroundColor: Colors.GRAY,
    },
    image: {
        height: 20,
        width: 20,
        resizeMode: 'contain'
    }
})