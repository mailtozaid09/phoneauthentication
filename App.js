import 'react-native-gesture-handler';
import * as React from 'react';
import { LogBox, StatusBar } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import LoginScreen from './src/screens/login/LoginScreen';
import VerifyScreen from './src/screens/login/VerifyScreen';

import HomeScreen from './src/screens/dashboard/HomeScreen';

import Colors from './src/styles/Colors';

LogBox.ignoreAllLogs(true);

const Stack = createStackNavigator();

const App = () => {
    return (
        <NavigationContainer>
            <StatusBar animated={true} backgroundColor={Colors.BLACK} />
            <Stack.Navigator initialRouteName="Login" >
                <Stack.Screen
                    name="Login"
                    component={LoginScreen}
                    options={{
                        headerShown: true,
                        headerStyle: {
                            backgroundColor: Colors.ORANGE,
                        },
                        headerTitleStyle: {
                            color: Colors.WHITE,
                            fontSize: 22
                        },
                        headerTitle: 'Login',
                        headerTitleAlign: 'center'
                    }}
                />
                <Stack.Screen 
                    name="Verify" 
                    component={VerifyScreen}
                    options={{
                        headerShown: true,
                        headerStyle: {
                            backgroundColor: Colors.ORANGE,
                        },
                        headerTitleStyle: {
                            color: Colors.WHITE,
                            fontSize: 22
                        },
                        headerTitle: 'Verify OTP',
                        headerTintColor: Colors.WHITE,
                        headerTitleAlign: 'center'
                    }}
                />
                <Stack.Screen 
                    name="Home" 
                    component={HomeScreen}
                    options={{
                        headerShown: true,
                        headerStyle: {
                            backgroundColor: Colors.ORANGE,
                        },
                        headerTitleStyle: {
                            color: Colors.WHITE,
                            fontSize: 22
                        },
                        headerTitle: 'Home',
                        headerTintColor: Colors.WHITE,
                        headerTitleAlign: 'center',
                        headerLeft: () => null,
                    }}
                />
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default App;
