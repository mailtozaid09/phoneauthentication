import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TextInput, ScrollView, TouchableOpacity,  } from 'react-native'

import Colors from '../../styles/Colors';
import Input from '../input/Input';

const LoginModal = (props) => {

    const { value, onChange } = props;

    return (
        <View style={styles.modalContainer}>
            <View style={styles.modalContent} >
                <Input
                    placeholder="(+91) India"
                    isEditable={false}
                />

                <Input
                    value={value}
                    placeholder="Enter your mobile number"
                    isEditable={true}
                    onChangeText={onChange}
                />

                <View style={styles.center} >
                    <View style={styles.otpTextContainer} >
                        <Text style={styles.otpTextStyle} >We will send you one time password (OTP)</Text>
                    </View>
                </View>
                
            </View>
            
        </View> 
    )
}

const styles = StyleSheet.create({
    modalContainer: {
        alignItems: 'center', 
        height: 280,
        flex: 1,
    },
    modalContent: {
        flex: 1,
        position: 'absolute', 
        top: -40, 
        width: 300, 
        height: 280, 
        borderRadius: 20,
        padding: 20,
        backgroundColor: Colors.WHITE
    },
    otpTextContainer: {
        width: '70%',
    },
    otpTextStyle: {
        fontSize: 14,
        lineHeight: 14,
        textAlign: 'center',
        color: Colors.BLACK
    },
    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})

export default LoginModal;