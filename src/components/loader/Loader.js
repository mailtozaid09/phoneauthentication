import React, { Component } from 'react'
import { Text, View, ActivityIndicator, } from 'react-native'
import Colors from '../../styles/Colors'


const Loader = () => {
    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} >
            <ActivityIndicator size="large" color={Colors.ORANGE} />
        </View>
    )
}

export default Loader
