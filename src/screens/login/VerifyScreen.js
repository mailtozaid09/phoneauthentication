import React, { useState, useEffect, useRef } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TextInput, ScrollView, TouchableOpacity,  } from 'react-native'

import Colors from '../../styles/Colors';

import Button from '../../components/button/Button';
import ErrorToast from '../../components/toast/ErrorToast';

import firebase from 'react-native-firebase';

const {width} = Dimensions.get('window');

import { useNavigation } from '@react-navigation/native';

const VerifyScreen = (props) => {

    const navigation = useNavigation();

    const inputRef1 = useRef();
    const inputRef2 = useRef();
    const inputRef3 = useRef();
    const inputRef4 = useRef();
    const inputRef5 = useRef();


    const [number, setNumber] = useState(props.route.params.num);
    const [otpValue, setOtpValue] = useState('');
    const [confirmResult, setConfirmResult] = useState(null);
    
    const [butonLoader, setButonLoader] = useState(false);
    const [error, setError] = useState(false);
    const [errorMsg, setErrorMsg] = useState('');

    const [input1, setInput1] = useState('');
    const [input2, setInput2] = useState('');
    const [input3, setInput3] = useState('');
    const [input4, setInput4] = useState('');
    const [input5, setInput5] = useState('');
    const [input6, setInput6] = useState('');

    useEffect(() => {
        console.log("----Verfiy-----");
        handleSendCode()
    }, [])
    


    const handleSendCode = () => {
        var num = '+91'+ number
        firebase
        .auth()
        .signInWithPhoneNumber(num)
        .then(confirmResult => {
            console.log(confirmResult);
            setConfirmResult(confirmResult)
        })
        .catch(error => {
            alert(error.message)
    
            console.log(error)
        })
    }

    const verifyOtp = () => {
        console.log("verifyOtp");

        setButonLoader(true)

        if(input1 && input2 && input3 && input4 && input5 && input6){

            var otp = input1 + input2 + input3 + input4 + input5 + input6
            console.log("OTP ___   " + otp);

            if (otp.length == 6) {
                confirmResult
                .confirm(otp)
                .then(user => {
                    navigation.navigate('Home')
                    setButonLoader(false)
                })
                .catch(error => {
                    setError(true);
                    setErrorMsg('Invalid OTP!');
                    setButonLoader(false)
                    console.log(error)
                })
            } else {
                alert('Please enter a 6 digit OTP code.')
            }
        }else{
            setError(true);
            setErrorMsg('Please enter value!');
            setButonLoader(false)
        }
    }

    const resendOtp = () => {
        console.log("resendOtp");

        setError(true);
        setErrorMsg('OTP sent!');
        
        setTimeout(() => {
            setError(false);
            setErrorMsg('');
        }, 1500);
        
        setInput1('');
        setInput2('');
        setInput3('');
        setInput4('');
        setInput5('');
        setInput6('');

        handleSendCode()
    }

    return (
        <View style={styles.container} >
            <View style={styles.content} >

                <View style={styles.center} >
                    <Text style={styles.titleStyle} >Please enter the 6-digit code sent to you at +91-{number}</Text>
                   

                    <View style={{marginTop: 50, marginBottom: 20, width: '90%', justifyContent: 'space-between', flexDirection: 'row'}} >
                        <View  style={styles.inputContainer}>
                            <TextInput
                                textAlign={'center'}
                                keyboardType="numeric"
                                maxLength={1}
                                value={input1}
                                onSubmitEditing={() => inputRef1.current.focus()}
                                onChangeText={(text) => {setInput1(text); setError(false); setErrorMsg('');}}
                            />
                        </View>
                        <View  style={styles.inputContainer}>
                            <TextInput
                                textAlign={'center'}
                                keyboardType="numeric"
                                maxLength={1}
                                value={input2}
                                ref={inputRef1}
                                onSubmitEditing={() => inputRef2.current.focus()}
                                onChangeText={(text) => {setInput2(text); setError(false); setErrorMsg('');}}
                            />
                        </View>
                        <View  style={styles.inputContainer}>
                            <TextInput
                                textAlign={'center'}
                                keyboardType="numeric"
                                maxLength={1}
                                value={input3}
                                ref={inputRef2}
                                onSubmitEditing={() => inputRef3.current.focus()}
                                onChangeText={(text) => {setInput3(text); setError(false); setErrorMsg('');}}
                            />
                        </View>
                        <View  style={styles.inputContainer}>
                            <TextInput
                                textAlign={'center'}
                                keyboardType="numeric"
                                maxLength={1}
                                value={input4}
                                ref={inputRef3}
                                onSubmitEditing={() => inputRef4.current.focus()}
                                onChangeText={(text) => {setInput4(text); setError(false); setErrorMsg('');}}
                            />
                        </View>
                        <View  style={styles.inputContainer}>
                            <TextInput
                                textAlign={'center'}
                                keyboardType="numeric"
                                maxLength={1}
                                value={input5}
                                ref={inputRef4}
                                onSubmitEditing={() => inputRef5.current.focus()}
                                onChangeText={(text) => {setInput5(text); setError(false); setErrorMsg('');}}
                            />
                        </View>
                        <View  style={styles.inputContainer}>
                            <TextInput
                                textAlign={'center'}
                                keyboardType="numeric"
                                maxLength={1}
                                value={input6}
                                ref={inputRef5}
                                onChangeText={(text) => {setInput6(text); setError(false); setErrorMsg('');}}
                            />
                        </View>

                    </View>


                    <View style={styles.resendContainer} >
                        <Text style={styles.resendStyle} >Didn't receive a OTP? <Text onPress={() => resendOtp()}  style={styles.resendOTPStyle} >Resend OTP</Text></Text>
                    </View>

                </View>

                <View style={styles.center} >
                    {error
                    ?
                    <View style={{width: '60%'}} >
                        <ErrorToast message={errorMsg} />
                    </View>
                    :
                    null
                    }
                    <Button 
                        loader={butonLoader}
                        title="VERIFY" 
                        onPress={() => verifyOtp()}
                    />
                </View>
            </View>

            
        </View>
    )
}

export default VerifyScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.ORANGE,
    },
    inputContainer: {
        height: 40, 
        width: 40, 
        backgroundColor: Colors.LIGHT_ORANGE, 
        borderRadius: 10,
        alignItems: 'center',
    },
    content: {
        flex: 1,
        paddingTop: 10,
        padding: 20,
        justifyContent: 'space-between',
    },
    titleStyle: {
        fontSize: 18,
        textAlign: 'center',
        color: Colors.WHITE
    },
    resendContainer: {
        alignItems: 'center'
    },
    resendStyle: {
        fontSize: 14,
        color: Colors.WHITE
    },
    resendOTPStyle: {
        fontSize: 14,
        color: Colors.WHITE,
        textDecorationLine: "underline",
        textDecorationStyle: "solid",
        textDecorationColor: "#000",
    },
    center: {
        alignItems: 'center'
    }
})