import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TextInput, ScrollView, TouchableOpacity, ActivityIndicator } from 'react-native'

import Colors from '../../styles/Colors';
import Input from '../input/Input';

const {width} = Dimensions.get('window');

const Button = (props) => {
    return (
        <View style={styles.container}>
           

            {props.loader
            ?
            <View style={[styles.button, {alignItems: 'center', justifyContent: 'center'}]} >
                <ActivityIndicator size="large" color={Colors.ORANGE} />
            </View>
            :
            <TouchableOpacity onPress={props.onPress} style={styles.button} >
                <Text style={styles.text} >{props.title}</Text>
            </TouchableOpacity>
            }
        </View> 
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center', 
        width: width-40,
    },
    buttonContainer: {
        position: 'absolute', 
        bottom: 0, 
        width: 80, 
        height: 80, 
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: { 
        height: 50, 
        width: '100%', 
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.WHITE,
    },
    otpTextContainer: {
        marginTop: 20,
        width: '70%',
    },
    otpTextStyle: {
        fontSize: 14,
        textAlign: 'center',
        color: Colors.BLACK
    },
    center: {
        alignItems: 'center'
    },
    image: {
        height: 30,
        width: 30,
        resizeMode: 'contain'
    },
    text: {
        fontSize: 22,
        color: Colors.ORANGE
    }
    
})

export default Button;