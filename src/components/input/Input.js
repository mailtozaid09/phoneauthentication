import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TextInput, ScrollView, TouchableOpacity,  } from 'react-native'

import Colors from '../../styles/Colors';

const Input = (props) => {

    const { value, placeholder, onChangeText, isEditable } = props;
    return (
        <View style={styles.container}>
            <TextInput
                editable={isEditable}
                placeholder={placeholder}
                keyboardType='numeric'
                maxLength={10}
                placeholderTextColor={!isEditable ? Colors.BLACK : null}
                value={value}
                style={styles.text}
                onChangeText={onChangeText}
                underlineColorAndroid={Colors.GRAY}
            />
        </View> 
    )
}

const styles = StyleSheet.create({
    container: {
    },
    text: {
        fontSize: 16,
    }
})


export default Input