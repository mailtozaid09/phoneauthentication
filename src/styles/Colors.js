const Colors = {
    RED: 'red',
    BLACK: '#000',
    WHITE: '#fff',
    GRAY: '#C0C0C0',
    
    BLUE: '#1D1690',
    ORANGE: '#f29b43',
    DARK_BLUE: '#0d3bc7',
    DARK_GRAY: '#6C757D',
    LIGHT_GRAY: '#d4d4d4',
    LIGHT_BLACK: '#212121',
    LIGHT_ORANGE: '#f0c59c'
}

export default Colors