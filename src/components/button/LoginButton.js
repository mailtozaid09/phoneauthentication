import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TextInput, ScrollView, TouchableOpacity,  } from 'react-native'

import Colors from '../../styles/Colors';
import Input from '../input/Input';

const LoginButton = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.buttonContainer} >
                <TouchableOpacity onPress={props.onPress} style={styles.button} >
                    <Image source={require('../../../assets/next.png')} style={styles.image} />
                </TouchableOpacity>
            </View>
        </View> 
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center', 
    },
    buttonContainer: {
        position: 'absolute', 
        bottom: 0, 
        width: 80, 
        height: 80, 
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:  '#f5f5f5',
    },
    button: { 
        height: 60, 
        width: 60, 
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.ORANGE,
    },
    otpTextContainer: {
        marginTop: 20,
        width: '70%',
    },
    otpTextStyle: {
        fontSize: 14,
        lineHeght: 14,
        textAlign: 'center',
        color: Colors.BLACK
    },
    center: {
        alignItems: 'center'
    },
    image: {
        height: 30,
        width: 30,
        resizeMode: 'contain'
    }
    
})

export default LoginButton;