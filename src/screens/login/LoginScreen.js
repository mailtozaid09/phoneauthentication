import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, Dimensions, TextInput, ScrollView, TouchableOpacity,  } from 'react-native'

import Colors from '../../styles/Colors';
import Input from '../../components/input/Input';

import LoginModal from '../../components/login/LoginModal';
import LoginButton from '../../components/button/LoginButton';
import Loader from '../../components/loader/Loader';
import ErrorToast from '../../components/toast/ErrorToast';

import firebase from 'react-native-firebase';

const LoginScreen = ({navigation}) => {

    const [number, setNumber] = useState('');
    const [loader, setLoader] = useState(true);
    const [error, setError] = useState(false);
    const [errorMsg, setErrorMsg] = useState('');


    useEffect(() => {
      
        firebase.auth().onAuthStateChanged(user => {
            if (user) {
                console.log("USER LOGGED IN");
                navigation.navigate('Home')
                setLoader(false)
            } else {
                console.log("NO USER LOGGED IN");
                setLoader(false)
            }
        });
    }, [])


    const checkNumber = () => {

        if(number.length === 10){
            navigation.navigate('Verify', {num: number} )
        }else{
            setError(true)
            setErrorMsg('Invalid Number!')
        }
        
    }


    const renderContent = () => {
        return(
            <ScrollView>
                <View style={styles.imageContainer}>
                    <Image
                        source={require('../../../assets/login.png')}
                        style={styles.imageStyle}
                    />
                </View>
                
                <View style={styles.modalContainer}>
                    <View style={styles.modalContent} >
                        <Input
                            placeholder="(+91) India"
                            isEditable={false}
                        />

                        <Input
                            value={number}
                            placeholder="Enter your mobile number"
                            isEditable={true}
                            onChangeText={(text) => {
                                setNumber(text);
                                setError(false)
                                setErrorMsg('')
                            }}
                        />

                        <View style={styles.center} >
                            <View style={styles.otpTextContainer} >
                                <Text style={styles.otpTextStyle} >We will send you one time password (OTP)</Text>
                            </View>
                        </View>
                        
                    </View>
                </View> 

                <LoginButton onPress={() => {
                    checkNumber()
                }} />


                
            </ScrollView>
        )
    }
 
    return (
        <View style={styles.container} >
            {loader
            ?
            <Loader/>
            :
            <View>
                {renderContent()}
                <View style={{height: 100, width: '100%', alignItems: 'center', position: 'absolute', bottom: -120}} >
                    {error
                    ?
                    <View style={{width: '60%'}} >
                        <ErrorToast message={errorMsg} />
                    </View>
                    :
                    null
                    }
                </View>
            </View>
            }
        </View>
    )
}

export default LoginScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f5f5f5'
    },
    imageContainer: {
        backgroundColor: Colors.ORANGE,
        alignItems: 'center'
    },
    imageStyle: {
        height: 250,
        resizeMode: 'contain'
    },
    modalContainer: {
        alignItems: 'center', 
        height: 280,
        flex: 1,
    },
    modalContent: {
        flex: 1,
        position: 'absolute', 
        top: -40, 
        width: 300, 
        height: 280, 
        borderRadius: 20,
        padding: 20,
        backgroundColor: Colors.WHITE
    },
    otpTextContainer: {
        width: '70%',
    },
    otpTextStyle: {
        fontSize: 14,
        textAlign: 'center',
        color: Colors.BLACK
    },
    center: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})