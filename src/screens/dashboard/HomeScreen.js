import React, { useEffect } from 'react'
import { Text, View, StyleSheet, Image, BackHandler, Dimensions, TextInput, ScrollView, TouchableOpacity,  } from 'react-native'

import Colors from '../../styles/Colors';
import firebase from 'react-native-firebase';

import Button from '../../components/button/Button';
import LottieView from 'lottie-react-native';

const {width} = Dimensions.get('window');

const HomeScreen = ({navigation}) => {

    useEffect(() => {       
        BackHandler.addEventListener('hardwareBackPress', backButtonHandler);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', backButtonHandler);
        };
    }, [])
    
    
    const backButtonHandler = () => {
        BackHandler.exitApp();
        return true;
    };
    

    return (
        <View style={styles.container} >
                <Text style={styles.welcome} >Welcome ! ! !</Text>
              <View style={styles.lottie}>
                    <LottieView source={require('../../../assets/welcome.json')} autoPlay loop />
                </View>
            <View style={styles.button} >
                <Button 
                    title="LOG OUT" 
                    onPress={() => {
                        firebase.auth().signOut();
                        navigation.navigate('Login')
                    }}
                />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.ORANGE
    },
    button: {
        position: 'absolute',
        bottom: 20
    },
    lottie: {
        width: width*0.8,
        height: width*0.8,
        marginBottom: 60, 
    },
    welcome: {
        fontSize: 28,
        color: Colors.WHITE
    }
})

export default HomeScreen